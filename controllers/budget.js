const connection = require("../connection");

/*
  Route: /budget/create
  Will create a new monthly budget for the user.
*/
exports.create = async (req, res) => {
  const query =
    "INSERT INTO MonthlyBudget(user_id, month, year, amount) VALUES (?, ? ,?, ?)";

  const params = [
    req.user.user_id,
    req.body.month,
    req.body.year,
    req.body.amount,
  ];

  //Connect to the database and run the query
  connection.query(query, params, (error, result) => {
    if (error) {
      console.log(error);
    }
    res.send({
      ok: true,
      id: result.insertId,
    });
  });
};

/*
  Route: /budget/:user_id/allbudgets
  Selects all budgets for a user.
*/
exports.allBudgets = async (req, res) => {
  const query = "SELECT * FROM MonthlyBudget WHERE user_id = ?";
  const params = [req.params.user_id];

  return new Promise((resolve, reject) => {
    connection.query(query, params, (error, results) => {
      if (error) {
        console.log(error);
      }
      res.send({
        ok: true,
        budgets: results,
      });
    });
  });
};

/*
  Route: /budget/delete
  Deletes the monthly budget, as well as all records associated.
*/
exports.delete = async (req, res) => {
  const budgetQuery =
    "DELETE MonthlyBudget, MonthlyBudgetCategory, Category FROM MonthlyBudget INNER JOIN MonthlyBudgetCategory ON MonthlyBudget.budget_id = MonthlyBudgetCategory.budget_id INNER JOIN Category ON MonthlyBudgetCategory.category_id = Category.category_id WHERE MonthlyBudget.budget_id = ?";
  const budgetParams = [req.body.budget_id];

  // Delete MonthlyBudgetCategory linking records
  connection.query(budgetQuery, budgetParams, (error, results) => {
    if (error) {
      console.log(error);
    }
    res.send({
      ok: true,
    });
  });
};

/*
  Route: /budget/edit
  Updates the budget with the given information.
*/
exports.edit = async (req, res) => {
  const query =
    "UPDATE MonthlyBudget SET month = ?, year = ?, amount = ? WHERE budget_id = ?";
  const params = [
    req.body.month,
    req.body.year,
    req.body.amount,
    req.body.budget_id,
  ];
  connection.query(query, params, (error, result) => {
    if (error) {
      console.log(error);
    }
    res.send({
      ok: true,
    });
  });
};

/*
  Route: /budget/:budget_id/allexpenses
  Returns all existing expenses tied to a particular budget_id.
*/
exports.allExpenses = async (req, res) => {
  const query =
    "SELECT MonthlyBudget.budget_id, Category.category_id, Category.name as 'category_name', Expense.expense_id, Expense.name, Expense.cost FROM MonthlyBudget INNER JOIN MonthlyBudgetCategory ON MonthlyBudget.budget_id = MonthlyBudgetCategory.budget_id INNER JOIN Category ON MonthlyBudgetCategory.category_id = Category.category_id INNER JOIN Expense ON Expense.category_id = Category.category_id WHERE MonthlyBudget.budget_id = ?";
  const params = [req.params.budget_id];

  return new Promise((resolve, reject) => {
    connection.query(query, params, (error, results) => {
      if (error) {
        console.log(error);
      }
      res.send({
        ok: true,
        allExpenses: results,
      });
    });
  });
};
