const connection = require("../connection");

/*
  Route: /category/create
  Creates the new category, given the name/amount of budget.
*/
exports.create = async (req, res) => {
  const categoryQuery =
    "INSERT INTO Category(name, amount_of_budget) VALUES (?, ?)";
  const monthlyBudgetCategoryQuery =
    "INSERT INTO MonthlyBudgetCategory(user_id, budget_id, category_id) VALUES (?, ?, ?)";
  const categoryParams = [req.body.name, req.body.amount_of_budget];

  connection.query(categoryQuery, categoryParams, (error, result) => {
    if (error) {
      console.log(error);
    }

    const monthlyBudgetCategoryParams = [
      req.user.user_id,
      req.body.budget_id,
      result.insertId,
    ];

    connection.query(
      monthlyBudgetCategoryQuery,
      monthlyBudgetCategoryParams,
      (error, result2) => {
        if (error) {
          console.log(error);
        }
        res.send({
          ok: true,
          id: result.insertId,
        });
      }
    );
  });
};

/*

  Route: /category/all
  get all categories for a budget
*/
exports.allUserCategories = async (req, res) => {
  const query =
    "SELECT Category.category_id, Category.amount_of_budget, Category.name, MonthlyBudgetCategory.budget_id FROM Category INNER JOIN MonthlyBudgetCategory ON MonthlyBudgetCategory.category_id = Category.category_id WHERE MonthlyBudgetCategory.budget_id = ? AND MonthlyBudgetCategory.user_id = ?";
  const params = [req.params.budget_id, req.params.user_id];

  connection.query(query, params, (error, results) => {
    if (error) {
      console.log(error);
    }
    res.send({
      ok: true,
      categories: results,
    });
  });
};

/*
  Route: /category/edit
  Updates the category_id with the name and amount of budget.
*/
exports.edit = async (req, res) => {
  const query =
    "UPDATE Category SET name = ?, amount_of_budget = ? WHERE category_id = ?";
  const params = [
    req.body.name,
    req.body.amount_of_budget,
    req.body.category_id,
  ];
  //Updates Category on category_id
  connection.query(query, params, (error, result) => {
    if (error) {
      console.log(error);
    }
    res.send({
      ok: true,
    });
  });
};

/*
  Route: /category/delete
  Deletes the category from Category and related records from MonthlyBudgetCategory
*/
exports.delete = async (req, res) => {
  const categoryQuery = "DELETE FROM Category WHERE category_id = ?";
  const monthlyBudgetCategoryQuery =
    "DELETE FROM MonthlyBudgetCategory WHERE category_id = ?";
  const params = [req.body.category_id];

  //Remove this category from all monthly budgets
  connection.query(monthlyBudgetCategoryQuery, params, (error, result) => {
    if (error) {
      console.log(error);
    }
    //Deletes category from category table
    connection.query(categoryQuery, params, (error, result) => {
      if (error) {
        console.log(error);
      }
      res.send({
        ok: true,
      });
    });
  });
};
