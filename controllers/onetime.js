const connection = require("../connection");

/*
  Route: /onetime/create
  Creates a new One Time Expense.
*/
exports.create = async (req, res) => {
  const expQuery =
    "INSERT INTO Expense(user_id, category_id, name, cost) VALUES (?, ? ,?, ?)";
  const expParams = [
    req.user.user_id,
    req.body.category_id,
    req.body.name,
    req.body.cost,
  ];
  const oneQuery =
    "INSERT INTO OneTime(expense_id, category_id, user_id, date) VALUES (?, ?, ?, ?)";

  //Connect to the database and run the query
  connection.query(expQuery, expParams, (error, expResult) => {
    if (error) {
      console.log(error);
    }

    const oneParams = [
      expResult.insertId,
      req.body.category_id,
      req.user.user_id,
      req.body.date,
    ];

    connection.query(oneQuery, oneParams, (error2, oneResult) => {
      if (error2) {
        console.log(error2);
      }
      res.send({
        ok: true,
        expense_id: expResult.insertId,
        onetime_id: oneResult.insertId,
      });
    });
  });
};

/*
  Route: /onetime/edit
  Edits a currently existing one time expense.
*/
exports.edit = async (req, res) => {
  const expQuery = "UPDATE Expense SET name = ?, cost = ? WHERE expense_id = ?";
  const oneQuery = "UPDATE OneTime SET date = ? WHERE onetime_id = ?";

  const expParams = [req.body.name, req.body.cost, req.body.expense_id];
  const oneParams = [req.body.date, req.body.onetime_id];

  //Updates Expenses on expense_id, then updates OneTime on onetime_id
  connection.query(expQuery, expParams, (error, result) => {
    if (error) {
      console.log(error);
    }
    connection.query(oneQuery, oneParams, (error2, result) => {
      if (error2) {
        console.log(error2);
      }
      res.send({
        ok: true,
      });
    });
  });
};

/*
  Route: /onetime/delete
  Deletes a currently existing one time expense.
*/
exports.delete = async (req, res) => {
  const oneQuery = "DELETE FROM OneTime WHERE onetime_id = ?";
  const expQuery = "DELETE FROM Expense WHERE expense_id = ?";
  const oneParams = [req.body.onetime_id];
  const expParams = [req.body.expense_id];

  //Deletes from OneTime, then Expenses.
  connection.query(oneQuery, oneParams, (error, result) => {
    if (error) {
      console.log(error);
    }
    connection.query(expQuery, expParams, (error2, result) => {
      if (error2) {
        console.log(error2);
      }
      res.send({
        ok: true,
      });
    });
  });
};

/*
  Route: /onetime/:user_id/:budget_id
  Returns all currently existing one time expenses tied to a particular budget_id.
*/
exports.allOneTime = async (req, res) => {
  const query =
    "SELECT MonthlyBudget.budget_id, Category.category_id, Category.name as 'category_name', Expense.expense_id, Expense.name, Expense.cost, OneTime.onetime_id, OneTime.date FROM MonthlyBudget INNER JOIN MonthlyBudgetCategory ON MonthlyBudget.budget_id = MonthlyBudgetCategory.budget_id INNER JOIN Category ON MonthlyBudgetCategory.category_id = Category.category_id INNER JOIN Expense ON Expense.category_id = Category.category_id INNER JOIN OneTime ON Expense.expense_id = OneTime.expense_id WHERE MonthlyBudget.budget_id = ?";
  const params = [req.params.budget_id];

  return new Promise((resolve, reject) => {
    connection.query(query, params, (error, results) => {
      if (error) {
        console.log(error);
      }
      res.send({
        ok: true,
        oneTimeExpenses: results,
      });
    });
  });
};
