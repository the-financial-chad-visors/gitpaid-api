const connection = require("../connection");

/*
  Route: /recurring/create
  Creates a recurring expense.
*/
exports.create = async (req, res) => {
  const expQuery =
    "INSERT INTO Expense(user_id, category_id, name, cost) VALUES (?, ? ,?, ?)";
  const expParams = [
    req.user.user_id,
    req.body.category_id,
    req.body.name,
    req.body.cost,
  ];
  const recQuery =
    "INSERT INTO Recurring(expense_id, category_id, user_id, renewal_date) VALUES (?, ?, ?, ?)";

  //Connect to the database and run the query
  connection.query(expQuery, expParams, (error, expResult) => {
    if (error) {
      console.log(error);
    }

    const recParams = [
      expResult.insertId,
      req.body.category_id,
      req.user.user_id,
      req.body.renewal_date,
    ];

    return new Promise((resolve, reject) => {
      connection.query(recQuery, recParams, (error2, recResult) => {
        if (error2) {
          console.log(error2);
        }
        res.send({
          ok: true,
          expense_id: expResult.insertId,
          recurring_id: recResult.insertId,
        });
      });
    });
  });
};

/*
  Route: /recurring/edit
  Updates a currently existing recurring expense.
*/
exports.edit = async (req, res) => {
  const expQuery = "UPDATE Expense SET name = ?, cost = ? WHERE expense_id = ?";
  const recQuery =
    "UPDATE Recurring SET renewal_date = ? WHERE recurring_id = ?";

  const expParams = [req.body.name, req.body.cost, req.body.expense_id];
  const recParams = [req.body.renewal_date, req.body.recurring_id];

  //Updates Expenses on expense_id, then updates OneTime on onetime_id
  connection.query(expQuery, expParams, (error, expResult) => {
    if (error) {
      console.log(error);
    }
    connection.query(recQuery, recParams, (error2, recResult) => {
      if (error2) {
        console.log(error2);
      }
      res.send({
        ok: true,
      });
    });
  });
};

/*
  Route: /recurring/delete
  Deletes a currently existing recurring expense.
*/
exports.delete = async (req, res) => {
  const recQuery = "DELETE FROM Recurring WHERE recurring_id = ?";
  const expQuery = "DELETE FROM Expense WHERE expense_id = ?";
  const recParams = [req.body.recurring_id];
  const expParams = [req.body.expense_id];

  //Deletes from Recurring, then from Expenses.
  connection.query(recQuery, recParams, (error, recResult) => {
    if (error) {
      console.log(error);
    }
    connection.query(expQuery, expParams, (error2, expResult) => {
      if (error2) {
        console.log(error2);
      }
      res.send({
        ok: true,
      });
    });
  });
};

/*
  Route: /recurring/:user_id/:budget_id
  Returns all of the recurring expenses of a particular budget.
*/
exports.allRecurring = async (req, res) => {
  const query =
    "SELECT MonthlyBudget.budget_id, Category.category_id, Category.name as 'category_name', Expense.expense_id, Expense.name, Expense.cost, Recurring.renewal_date, Recurring.recurring_id FROM MonthlyBudget INNER JOIN MonthlyBudgetCategory ON MonthlyBudget.budget_id = MonthlyBudgetCategory.budget_id INNER JOIN Category ON MonthlyBudgetCategory.category_id = Category.category_id INNER JOIN Expense ON Expense.category_id = Category.category_id INNER JOIN Recurring ON Expense.expense_id = Recurring.expense_id WHERE MonthlyBudget.budget_id = ?";
  const params = [req.params.budget_id];

  return new Promise((resolve, reject) => {
    connection.query(query, params, (error, results) => {
      if (error) {
        console.log(error);
      }
      res.send({
        ok: true,
        recurringExpenses: results,
      });
    });
  });
};
