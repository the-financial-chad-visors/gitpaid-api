const promiseRouter = require("express-promise-router");
const router = promiseRouter();
const authenticateToken = require("./middleware/authenticateJWT");

//User routes
const user = require("./controllers/user");
router.post("/user/signup", user.create);
router.post("/user/login", user.login);
router.patch("/user/edit", authenticateToken, user.edit);
router.post("/user/darkTheme", authenticateToken, user.darkTheme);
router.get("/user", authenticateToken, user.get);

const budget = require("./controllers/budget");
router.post("/budget/create", authenticateToken, budget.create);
router.delete("/budget/delete", authenticateToken, budget.delete);
router.patch("/budget/edit", authenticateToken, budget.edit);
router.get(
  "/budget/:budget_id/allexpenses",
  authenticateToken,
  budget.allExpenses
);
router.get("/budget/:user_id/allbudgets", authenticateToken, budget.allBudgets);

const category = require("./controllers/category");
router.post("/category/create", authenticateToken, category.create);
router.delete("/category/delete", authenticateToken, category.delete);
router.patch("/category/edit", authenticateToken, category.edit);
router.get(
  "/category/:user_id/:budget_id/all",
  authenticateToken,
  category.allUserCategories
);

const recurring = require("./controllers/recurring");
router.post("/recurring/create", authenticateToken, recurring.create);
router.delete("/recurring/delete", authenticateToken, recurring.delete);
router.patch("/recurring/edit", authenticateToken, recurring.edit);
router.get(
  "/recurring/:user_id/:budget_id",
  authenticateToken,
  recurring.allRecurring
);

const onetime = require("./controllers/onetime");
router.post("/onetime/create", authenticateToken, onetime.create);
router.delete("/onetime/delete", authenticateToken, onetime.delete);
router.patch("/onetime/edit", authenticateToken, onetime.edit);
router.get(
  "/onetime/:user_id/:budget_id",
  authenticateToken,
  onetime.allOneTime
);

module.exports = router;
