const fetch = require("node-fetch");
const expect = require("chai").expect;
const connection = require("../connection");
const host = "http://localhost:5000";

const defaultUser = {
  email: "test_user@email.com",
  password: "password",
};

let jwt;
let budget_id;

describe("Budgets", function () {
  before(async function () {
    let res = await fetch(`${host}/user/login`, {
      method: "POST",
      body: JSON.stringify(defaultUser),
      headers: { "Content-Type": "application/json" },
    });

    let data = await res.json();
    jwt = data.jwt;
  });

  describe("Create a new budget", function () {
    it("Should create a new budget", async function () {
      const body = {
        month: "February",
        year: 2000,
        amount: 500.5,
      };

      let res = await fetch(`${host}/budget/create`, {
        method: "POST",
        body: JSON.stringify(body),
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${jwt}`,
        },
      });

      let data = await res.json();
      budget_id = data.id;
      expect(data.ok).to.equal(true);
    });
  });

  describe("Edit a budget", function () {
    it("Should edit a budget", async function () {
      const body = {
        month: "January",
        year: 1998,
        amount: 200,
        budget_id: budget_id,
      };

      let res = await fetch(`${host}/budget/edit`, {
        method: "PATCH",
        body: JSON.stringify(body),
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${jwt}`,
        },
      });

      let data = await res.json();
      expect(data.ok).to.equal(true);
    });
  });

  describe("Delete a budget", function () {
    it("Should delete a budget", async function () {
      const body = {
        budget_id: budget_id,
      };

      let res = await fetch(`${host}/budget/delete`, {
        method: "DELETE",
        body: JSON.stringify(body),
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${jwt}`,
        },
      });

      let data = await res.json();
      expect(data.ok).to.equal(true);
    });
  });
});

function cleanDatabase(query, params) {
  //Clean up database
  connection.query(query, params, (error, result) => {
    if (error) {
      console.log(error);
    }
    return;
  });
}
