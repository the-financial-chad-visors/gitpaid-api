const fetch = require("node-fetch");
const expect = require("chai").expect;
const connection = require("../connection");
const host = "http://localhost:5000";

const defaultUser = {
  email: "test_user@email.com",
  password: "password",
};

let jwt;
let budget_id;
let category_id;

describe("Categories", function () {
  before(async function () {
    let res = await fetch(`${host}/user/login`, {
      method: "POST",
      body: JSON.stringify(defaultUser),
      headers: { "Content-Type": "application/json" },
    });

    let data = await res.json();
    jwt = data.jwt;

    const body = {
      month: "February",
      year: 2000,
      amount: 500.5,
    };

    let budgetRes = await fetch(`${host}/budget/create`, {
      method: "POST",
      body: JSON.stringify(body),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${jwt}`,
      },
    });

    let budgetData = await budgetRes.json();
    budget_id = budgetData.id;
  });

  describe("Create a new category", async function () {
    it("Should create a new category", async function () {
      const body = {
        name: "Gas",
        amount: 60,
        budget_id: budget_id,
      };

      let res = await fetch(`${host}/category/create`, {
        method: "POST",
        body: JSON.stringify(body),
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${jwt}`,
        },
      });

      let data = await res.json();
      category_id = data.id;
      expect(data.ok).to.equal(true);
    });
  });

  describe("Edit a category", async function () {
    it("Should edit a category", async function () {
      const body = {
        name: "Food",
        amount: 50,
        category_id: category_id,
      };

      let res = await fetch(`${host}/category/edit`, {
        method: "PATCH",
        body: JSON.stringify(body),
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${jwt}`,
        },
      });

      let data = await res.json();
      expect(data.ok).to.equal(true);
    });
  });

  describe("Delete a category", async function () {
    it("Should delete a category", async function () {
      const body = {
        category_id: category_id,
      };

      let res = await fetch(`${host}/category/delete`, {
        method: "DELETE",
        body: JSON.stringify(body),
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${jwt}`,
        },
      });

      let data = await res.json();
      expect(data.ok).to.equal(true);
    });
  });

  after(function () {
    cleanDatabase(
      `DELETE FROM MonthlyBudget WHERE budget_id = ${budget_id}; DELETE FROM User WHERE email = 'test_user@email.com'`,
      []
    );
  });
});

function cleanDatabase(query, params) {
  //Clean up database
  connection.query(query, params, (error, result) => {
    if (error) {
      console.log(error);
    }
    return;
  });
}
