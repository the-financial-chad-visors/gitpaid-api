const fetch = require("node-fetch");
const expect = require("chai").expect;
const connection = require("../connection");
const host = "http://localhost:5000";

describe("User Accounts", function () {
  describe("Create a new account", function () {
    it("Should create a new account", async function () {
      const body = {
        first_name: "Test",
        last_name: "User",
        email: "test_user@email.com",
        password: "password",
      };

      let res = await fetch(`${host}/user/signup`, {
        method: "POST",
        body: JSON.stringify(body),
        headers: { "Content-Type": "application/json" },
      });

      let data = await res.json();
      expect(data.ok).to.equal(true);
    });

    it("Should not create an account that already exists", async function () {
      const body = {
        first_name: "Test",
        last_name: "User",
        email: "test_user@email.com",
        password: "password",
      };

      let res = await fetch(`${host}/user/signup`, {
        method: "POST",
        body: JSON.stringify(body),
        headers: { "Content-Type": "application/json" },
      });

      expect(res.status).to.equal(400);
    });
  });

  describe("Log in to an account", function () {
    it("Should successfully log a user in if the email and password are correct", async function () {
      const body = {
        email: "test_user@email.com",
        password: "password",
      };

      let res = await fetch(`${host}/user/login`, {
        method: "POST",
        body: JSON.stringify(body),
        headers: { "Content-Type": "application/json" },
      });

      let data = await res.json();
      expect(data.ok).to.equal(true);
    });

    it("Should not log in if the email doesn't exist", async function () {
      const body = {
        email: "adfjasdjkfbasbdfabshdfha@email.com",
        password: "password",
      };

      let res = await fetch(`${host}/user/login`, {
        method: "POST",
        body: JSON.stringify(body),
        headers: { "Content-Type": "application/json" },
      });

      expect(res.status).to.equal(401);
    });

    it("Should not log in if the password is incorrect", async function () {
      const body = {
        email: "test_user@email.com",
        password: "password1",
      };

      let res = await fetch(`${host}/user/login`, {
        method: "POST",
        body: JSON.stringify(body),
        headers: { "Content-Type": "application/json" },
      });

      expect(res.status).to.equal(401);
    });
  });
});

function cleanDatabase(query, params) {
  //Clean up database
  connection.query(query, params, (error, result) => {
    if (error) {
      console.log(error);
    }
  });
}
